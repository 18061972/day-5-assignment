﻿using Xunit;
using LoginLibrary;
namespace unitTestLibrary

{
    public class LoginTest
    {
        [Fact]
        public void TestCase()
        {
            //Suppose ,Enter By User
            string entered_input1 = "dev@email.com";    
            string entered_input2 = "1234567890";
            
            //If Enter Value is Correct then expexted Output
            string expected_output = "Login Successfully";
            
            //Pass Values to the method for checking and accept value of actual output
            string actual_output = LoginUser.CheckLogin(entered_input1, entered_input2);
            
            //Here we check the actual output is equal to expected output then the test case is passed
            Assert.Equal(actual_output,expected_output);
        }
    }

}